CC = g++
CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic -g
DEBUGGING_FLAGS = -g -O0
CFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)


chess: main.o Board.o Chess.o CreatePiece.o Bishop.o King.o Knight.o Pawn.o Queen.o Rook.o
	$(CC) -o chess main.o Board.o Chess.o CreatePiece.o Bishop.o King.o Knight.o Pawn.o Queen.o Rook.o

Board.o: Board.cpp Bishop.h Board.h King.h Knight.h Mystery.h Pawn.h Piece.h CreatePiece.h Queen.h Rook.h Terminal.h
	$(CC) -c Board.cpp $(CFLAGS)

Chess.o: Chess.cpp Board.h Chess.h Piece.h
	$(CC) -c Chess.cpp $(CFLAGS)

CreatePiece.o: CreatePiece.cpp Bishop.h Board.h Chess.h King.h Knight.h Mystery.h Pawn.h Piece.h CreatePiece.h Queen.h Rook.h
	$(CC) -c CreatePiece.cpp Pawn.cpp Bishop.cpp King.cpp Knight.cpp Queen.cpp Rook.cpp $(CFLAGS)

Bishop.o: Bishop.h Piece.h 
	$(CC) -c Bishop.cpp $(CFLAGS)

King.o: King.h Piece.h
	$(CC) -c King.cpp $(CFLAGS)

Knight.o: Knight.h Piece.h 
	$(CC) -c Knight.cpp $(CFLAGS)

Pawn.o: Pawn.h Piece.h         
	$(CC) -c Pawn.cpp $(CFLASG)  

Queen.o: Queen.h Piece.h   
	$(CC) -c Queen.cpp $(CFLAGS) 

Rook.o: Rook.h Piece.h     
	$(CC) -c Rook.cpp $(CFLAGS)  

main.o: main.cpp Bishop.h Board.h Chess.h King.h Knight.h Mystery.h Pawn.h Piece.h CreatePiece.h Queen.h Rook.h
	$(CC) -c main.cpp $(CFLAGS)

.PHONY: clean all
clean:
	rm -f *.o chess
