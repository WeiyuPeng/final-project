//Bishop.h

#ifndef BISHOP_H
#define BISHOP_H

#include <utility>
#include "Piece.h"

class Bishop : public Piece {
public:

  bool legal_move_shape(std::pair<char , char> start, std::pair<char , char> end) const;

   //DO NOT MODIFY 
  char to_ascii( void ) const { return is_white() ? 'B' : 'b'; }

private:
   //DO NOT MODIFY 
   Bishop( bool is_white ) : Piece( is_white ) {}

   friend Piece* create_piece( char );
};

#endif // BISHOP_H
