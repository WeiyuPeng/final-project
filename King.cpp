//King.cpp

#include "Piece.h"
#include "King.h"
#include <cmath>

using std::abs;

bool King::legal_move_shape(std::pair < char, char> start, std::pair <char, char> end) const {

  //return false if king tries to move more than 1 space left or right
  if (abs(end.first - start.first) > 1) {
    return false;
  }
  //return false if King tries to move more than 1 space up or down
  if (abs(end.second - start.second) > 1) {
    return false;
  }
  //return false if king tries to stay still
  if (start.first == end.first && start.second == end.second) {
    return false;
  }
  return true;

}
