#ifndef BOARD_H
#define BOARD_H

#include <iostream>
#include <map>
#include <vector>
#include "Piece.h"
#include "Pawn.h"
#include "Mystery.h"
#include "Rook.h"
#include "Knight.h"
#include "Bishop.h"
#include "Queen.h"
#include "King.h"

class Board
{
  // Throughout, we will be accessing board positions using an std::pair< char , char >.
  // The assumption is that the first value is the column with values in {'A','B','C','D','E','F','G','H'} (all caps)
  // and the second is the row, with values in {'1','2','3','4','5','6','7','8'}
  
 public:
  // Default constructor
  Board( void );

  //Destructor for Board Class
  ~Board(void) {
    for (std::map<std::pair<char,char>,Piece*>::iterator it = _occ.begin();
	 it != _occ.end(); ++it) {
      delete it->second;
    }
  }

  // Check if the piece designator is valid
  bool valid_desig( char piece_designator ) const;
  
  // Check if the given position is on the board
  bool valid_pos( std::pair< char, char> position) const;
  
  // Returns a const pointer to the piece at a prescribed location if it exists, or a NULL pointer if there is nothing there.
  const Piece* operator() ( std::pair< char , char > position ) const;
  
  // Returns a const pointer to the piece at a prescribed location of it exits, otherwise, return NULL pointer
  const Piece* have_piece(std::pair< char, char> position) const;
  
  // Attempts to add a new piece with the specified designator, at the given location.
  // Returns false if:
  // -- the designator is invalid,
  // -- the specified location is not on the board, or
  // -- if the specified location is occupied
  bool add_piece( std::pair< char , char > position , char piece_designator );
  
  // Attempts to remove from the board the piece at the given location.
  // Returns false if:
  // -- the specified location is not on the board, or
  // -- if no piece exist at the location
  bool remove_piece (std::pair< char, char> position);
  
  // Displays the board by printing it to stdout
  void display( void ) const;
  
  // Returns true if the board has the right number of kings on it
  bool has_valid_kings( void ) const;
  
  // Return true if the piece will not go off the board
  bool legal_path(std::pair< char, char> start, std::pair< char, char> end) const;
  
  // For the case that the starting and the ending location is occupied,
  // return true if the piece in the end position is an opposing piece of the start
  bool is_opp(std::pair< char, char> start, std::pair< char, char> end) const;

  // Return true if the path described by the start and end locations are not straight
  // , meaning not horizontal, vertical or diagonal move
  bool nonstraight(std::pair< char, char> start, std::pair< char, char> end) const;
  
  // Return true if the path between two positions is not occupied by any piece
  // only used by pieces that move horizontally, vertically or diagonally
  bool clear_path(std::pair< char, char> start, std::pair< char, char> end) const;

  // Return true if the move is legal
  bool all_legal(std::pair<char, char> start, std:: pair< char, char> end) const;

  
  /*helper functions for convenient location informations*/
  // Return the location of the king of the chosen color
  std::pair< char, char> loc_king(bool white) const;
  
  // Return the allias' pieces
  std::map<std::pair< char, char>, Piece*> al(bool white) const;
  
  // Return the opposing's pieces
  std::map<std::pair< char, char>, Piece*> op(bool white) const;
  
  // Return all the locations in between two locations
  // only used by pieces that move horizontally, vertically or diagonally
  std::vector<std::pair<char, char>> loc_between(std::pair< char, char> start, std::pair< char, char> end) const;


  /* helper functions for make move */
  // Return boolean false(should not move) if the mystery piece get checked by itself
  bool mystery_fake_move(std::pair< char, char> start, std::pair<char, char> end);
  
  // make_move for the mystery piece
  void mystery_move(std::pair< char, char> start, std::pair<char, char> end);
  
  // Return boolean false(should not move) if the pawn promotion get checked by itself
  bool pawn_fake_promotion(std::pair< char, char> start, std::pair<char, char> end);
  
  // make_move for the pawn promotion
  void pawn_promotion(std::pair< char, char> start, std::pair<char, char> end);

  
  /* helper functions for check, checkmate */
  bool in_mate(bool white) const;
  
  bool in_check(bool white) const;
  
  bool checking(std::pair<char,char> king, bool white) const;
  
  bool round_checking(std::pair<char,char>king, bool white) const;
  
  bool multi_checking(std::pair<char,char>king, bool white) const;
  
  std::pair<char, char> killer_pos(std::pair<char, char>king, bool white) const;
  
  bool capture_killer(std::pair<char, char>king, bool white) const;
  
  bool block_path(std::pair<char, char>king, bool white) const;
  
  
 private:
  // The sparse map storing the pieces, keyed off locations
  std::map< std::pair< char , char > , Piece* > _occ;
};

// Write the board state to an output stream
std::ostream& operator << ( std::ostream& os , const Board& board );

#endif // BOARD_H
