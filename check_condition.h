
#ifndef CHECK_CONDITION_H_
#define CHECK_CONDITION_H_
#include <iostream>
#include "check_condition.h"


	bool checking(std::pair<char,char>king,bool white);

	bool round_checking(std::pair<char,char>king,bool white);

	bool multi_checking(std::pair<char,char>king,bool white);

	std::pair<char,char> killer_pos(std::pair<char,char>king,bool white);

	bool capture_killer(std::pair<char,char>king,bool white);

	bool block_path(std::pair<char,char>king,bool white);


#endif /* CHECK_CONDITION_H_ */
