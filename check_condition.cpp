/* bool checkmate:
 * 0.check the king's position got checked or not
 * 1.(change the role first)check the legal path of the king. For example king is 5,
 * check the position availability of 1-8 exclude 5
 * 2.we might need a piece locator to find all the opposing piece's location.
 * 3.check the available position of 1-8 one by one. Check the got check function for each move.
 * if true move to next position. else exit checkmate condition and print fasle.
 * 3.5 as long as the king can't move, check how many pieces are checking the king at the same
 * time. If it's just one, do following. If more than one, checkmate.
 * 4.after all available position got checked, get the checking piece location. Get all the
 * position from that position to the king. check one by one if any of your piece can move to
 * that position(for those condition checked not by opposing knight or pawn).If checked by knight
 * or pawn, you have to check that if any of your piece can capture the checking piece. After
 * this move, use got_check function again to check if still got checked. If still checked, print
 * checkmate.
 *
 * bool check:
 * check if your pieces check opposing king or not. Checked call checkmate.
 *
 * bool got_checked:
 * check if your king got check or not. Return true if got checked.
 *
 * end round check:
 * (for every normal move, after every move, call check and got_check, if got_check returns true
 * go back one move and print not legal.
 *
 */
#include <iostream>
#include <utility>
#include <vector>
#include <map>
#include "Piece.h"
#include "Knight.h"
#include "Pawn.h"
#include "Bishop.h"
#include "Queen.h"
#include "King.h"
#include "Rook.h"
#include "Chess.h"
#include "Board.h"
#include "check_condition.h"

using std::get;
using std::pair;
using std::make_pair;
using std::abs;
using std::map;
using std::vector;
bool in_mate(bool white)
{
//check the king got checked or not
	pair<char,char> checked_king=loc_king(white);//get the location of the checked king
	if(checking(checked_king,white)==false)
	{return false;}//not even in checking condition,exit immediately
//check the position around the king
	if(round_checking(checked_king,white)==false)
	{return false;}//king can move to save himself
//right now, the king can't move
//check is there more than one piece are checking the king now
	if(multi_checking(checked_king,white)==true)
	{return true;}//if more than one piece is checking the king, it's checkmate,break
//check if your piece can capture the killer
	if(capture_killer(checked_king,white)==true)
	{return false;}//guards can capture the killer. not a checkmate
//check if your piece can block the piece at the path
	if(block_path(checked_king,white)==true)
	{return false;}//guards can go block the killer's path

	return true;
}

bool in_check(bool white)
{
	pair<char,char> checked_king=loc_king(white);//get the location of the checked king
	if(checking(checked_king,white)==false)
	{return false;}
	return true;
}

bool in_stalemate(bool white)
{	map<pair<char,char>,Piece*> killers=op(white);
	for(map<pair<char,char>,Piece*>::iterator it=killers.begin();it!=killers.end();++it)
	{	for(int j=0;j<8;j++)
		{	for(int i=0;i<8;i++)
			{pair<char,char>temp=make_pair('A'+i,'1'+j);
			 if(all_legal(*it,temp)==true)
			 {return false;}
			}
		}
	}
	return true;



}

bool checking(pair<char,char>king,bool white)
{
	map<pair<char,char>,Piece*> killers=op(white);
	for(map<pair<char,char>,Piece*>::iterator it=killers.begin();it!=killers.end();++it)
	{	if(it->second->legal_capture_shape(it->first,king)==true&&clear_path(it->first,king)==true)
			{return true;}//can be captured by someone
	}
	return false;
}

bool round_checking(pair<char,char>king,bool white)//return true if got checked all around
{
	pair<char,char>lower_row=make_pair(king.first-1,king.second-1);
	for(int j=0;j<3;j++)
		{for(int i=0;i<3;i++)
			{	pair<char,char>temp=make_pair(lower_row.first+i,lower_row.second+j);
				if(have_piece(temp)==NULL)//if position is empty and no piece is checking that, not checkmate
					{if(checking(temp, white)==false){return false;}}
				else
				{	if(is_opp(king,temp)==true)//king can capture the piece at that location
					{if(checking(temp, white)==false){return false;}}
				}
			}
		}
	return true;
}

bool multi_checking(pair<char,char>king,bool white)
{
	int occ=0;
	map<pair<char,char>,Piece*> killers=op(white);
	for(map<pair<char,char>,Piece*>::iterator it=killers.begin();it!=killers.end();++it)
	{	if(it->second->legal_capture_shape(it->first,king)==true&&clear_path(it->first,king)==true)
			{occ++;}//can be captured by someone
	}
	if(occ>1){return true;}
	return false;
}

pair<char,char> killer_pos(pair<char,char>king,bool white)
{	map<pair<char,char>,Piece*> killers=op(white);
	for(map<pair<char,char>,Piece*>::iterator it=killers.begin();it!=killers.end();++it)
	{	if(it->second->legal_capture_shape(it->first,king)==true&&clear_path(it->first,king)==true)
			{return it->first;}//can be captured by someone
	}
	return make_pair('?','?');
}

bool capture_killer(pair<char,char>king,bool white)
{
	pair<char,char>killer=killer_pos(king,white);
	map<pair<char,char>,Piece*> guards=al(white);
	for(map<pair<char,char>,Piece*>::iterator it=guards.begin();it!=guards.end();++it)
	{	if(it->second->legal_capture_shape(it->first,killer)==true&&clear_path(it->first,killer)==true)
			{return true;}//guards can captured the killer
	}
	return false;
}

bool block_path(pair<char,char>king,bool white)
{	map<pair<char,char>,Piece*> guards=al(white);
	pair<char,char>killer=killer_pos(king,white);
	vector<pair<char,char>>block_pos=loc_between(killer,king);
	for(vector<pair<char,char>>::iterator it=block_pos.begin();it!=block_pos.end();++it)
	{		for(map<pair<char,char>,Piece*>::iterator mit=guards.begin();mit!=guards.end();++mit)
			{ if(mit->second->legal_move_shape(mit->first,*it)==true&&clear_path(mit->first,*it)==true)
				{return true;}//guards can go block the killer's path
			}
	}
	return false;
}
