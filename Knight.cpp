//Knight.cpp

#include "Piece.h"
#include "Knight.h"
#include <utility>
#include <cmath>

using std::get;
using std::pair;
using std::abs;

bool Knight::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const
{
/*knight moves like this(k to x)
 *                                              |x|_|x|
 *                                              |_|_|_|
 *                                              |_|k|_|
 *                                              |_|_|_|
 *                                              |x|_|x|
 */
  if((abs(get<0>(start) - get<0>(end)) == 1) && abs(get<1>(start) - get<1>(end)) == 2) {
    return true;
  }
  /*knight moves like this(k to x)
   *                                        |x|_|_|_|x|
   *                                        |_|_|k|_|_|
   *                                        |x|_|_|_|x|
   */
  if(abs(get<0>(start) - get<0>(end))==2 && abs(get<1>(start) - get<1>(end)) == 1) {
    return true;
  }
  return false;
}

