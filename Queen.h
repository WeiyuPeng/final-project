//Queen.h
#ifndef QUEEN_H
#define QUEEN_H

#include "Piece.h"

class Queen : public Piece {
public:
  
  //check the shape of the specified move
  bool legal_move_shape( std::pair< char , char > start , std::pair< char , char > end) const;

  //DO NOT MODIFY 
  char to_ascii( void ) const { return is_white() ? 'Q' : 'q'; }

private:
       
  //DO NOT MODIFY
  Queen( bool is_white ) : Piece( is_white ) {}

  friend Piece* create_piece( char );
};

#endif // QUEEN_H
