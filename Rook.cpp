//Rook.cpp

#include "Piece.h"
#include "Rook.h"

bool Rook::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  //return true if Rook tries to move horizontally
  if (start.first == end.first) {
    return true;
  }
  //return true if Rook tries to move vertically
  if (start.second == end.second) {
    return true;
  }
  return false;
}
