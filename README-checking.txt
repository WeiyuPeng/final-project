bool in_mate:
as the first thing that got checked at the beginning of the round, in_mate will 
return true if checkmate to end the game. Otherwise, go to next check

1.check the king's position got checked or not,if not, exit the checkmate

2.check the legal path of the king. For example king is keyboard number 5,check the position 
availability of 1-8 exclude 5. If the position is out of the board or occupied by its allied
piece. King can not move to that position. Else, check if that position is also checked by 
opposing pieces. If there is one position that king can move to and not checked by opposing 
pieces, it's not a checkmate.

3. as long as the king can't move, check how many pieces are checking the king at the same
time. If it's just one, do following. If more than one, checkmate.

4.after all available position got checked, get the checking piece location. Get all the
position from that position to the king. check one by one if any of your piece can move to
that position(for those condition checked not by opposing knight or pawn).If checked by knight
or pawn, you have to check that if any of your piece can capture the checking piece. After
this move, use got_check function again to check if still got checked. If still checked, print
checkmate.

bool in_check:
check if your king's location has been checking by opposing pieces by calling function checking

bool got_checked:
we use this in prevending player to move the piece and let the king unprotected. Used in 
make_fake_move to check that movement's legality.
check if your king got check or not. Return true if got checked.

bool in_stalemate:
for this condition we try to access every piece you the current player has on the board and check
it one by one if there is any legal move for them by use that current location as start location 
and try every position on the board to be ending location to be sure it can make a move. Then we 
have to call make_fake_move to check that if this move will cause the king got exposed which is illegal.

 
