#include <iostream>
#include <utility>
#include <map>
#ifndef _WIN32
#include "Terminal.h"
#endif // !_WIN32
#include "Board.h"
#include "CreatePiece.h"
#include <string>
#include <cmath>

using std::pair;
using std::make_pair;
using std::map;
using std::string;
using std::cout;
using std::endl;
using std::vector;
using std::get;

//DO NOT MODIFY
Board::Board( void ){}

typedef map< pair < char, char>, Piece*>::iterator conItr;
typedef map< pair < char, char>, Piece*>::const_iterator conconItr;

const Piece* Board::operator()( std::pair< char , char > position ) const {
  map<pair<char, char>, Piece*>::const_iterator it = _occ.find(position);
  if (it != _occ.end()) {
    return it->second;
  } else {
    return NULL;
  }
}

/* valid_desig function returns true if piece is K,Q,B,N.R.P, or M */
bool Board::valid_desig(char piece_designator) const {
  piece_designator = toupper(piece_designator);
  string valid_str = "KQBNRPM";
  bool validity = false;
  for (size_t i = 0; i < valid_str.length(); i++) {
    if (piece_designator == valid_str.at(i)) {
      validity = true;
    }
  }
  return validity;
}

/* valid_pos function returns true if specified position if on the board A-H and 1-8 */
bool Board::valid_pos( pair< char, char> position) const {
  if (position.first >= 'A' && position.first <= 'H') {
    if (position.second >= '1' && position.second <= '8') {
      return true;
    }
  }
  return false;
}

/* add_piece function returns true if a piece is successfully added */
bool Board::add_piece( std::pair< char , char > position , char piece_designator) {
  if (valid_desig(piece_designator) != true) {
    return false;
  }
  if (valid_pos(position) != true) {
    return false;
  }
  if (have_piece(position) != NULL) {
    return false;
  }
  _occ[ position ] = create_piece( piece_designator );
  return true;
}

/*remove_piece function returns true if piece is successful;y removed  */
bool Board::remove_piece( pair< char, char> position) {
  if (valid_pos(position) != true) {
    return false;
  }
  if (have_piece(position) == NULL) {
    return false;
  }
  else {
    conItr rem = _occ.find(position);
    delete rem->second;
    _occ.erase(position);
    return true;
  }
}

/* has_valid_kings function returns true if board has both white and black king */
bool Board::has_valid_kings( void ) const {
  bool valid_king = false;
  bool valid_King = false;
  for (conconItr it = _occ.cbegin(); it != _occ.cend(); ++it) {
    if ((it->second) ->to_ascii() == 'K') {
      valid_King = true;
    }
    if ((it->second) ->to_ascii() == 'k') {
      valid_king = true;
    }
  }
  return valid_king && valid_King;
}

/* have_piece function returns true if there is a piece at the specified location */
const Piece* Board::have_piece( std::pair< char , char > position ) const {
  conconItr it = _occ.find(position);
  if (it != _occ.end()) {
    return it->second;
  } else {
    return NULL;
  }
}

/* display function prints out the current board state to the user. 
   Yellow for black pieces. White for white pieces */
void Board::display( void ) const
{
  bool bright = false;
  const string col_names("ABCDEFGH");
  const string row_names("12345678");
  string result;
  //append column headers across the top
  cout << "   ";

  for(string::const_iterator it = col_names.begin();
      it != col_names.end(); ++it) {
    Terminal::color_fg(bright, Terminal::Color::RED);
    cout << ' ' << *it << ' ';
  }
  cout << endl;
  //append header line below column header line
  cout << "   ";
  for(string::const_iterator it = col_names.begin();
      it != col_names.end(); ++it) {
    Terminal::color_fg(bright, Terminal::Color::RED);
    
    cout << "---";
  }
  cout << endl;
  
  for(char r='8'; r >= '1'; r--) {
     Terminal::color_fg(bright, Terminal::Color::RED);
      cout << r << ' ' << '|';
      for( char c='A' ; c<='H' ; c++ ) {
       	Terminal::color_bg(Terminal::Color::YELLOW);
        if (abs(r-c)%2) {
	  Terminal::color_bg(Terminal::Color::MAGENTA);
	}
	  cout << ' ';
	  const Piece* piece = have_piece( pair< char , char> ( c , r ) );
	  if( piece && piece->is_white()) {
	    Terminal::color_fg(bright, Terminal::Color::WHITE);
	    cout << piece->to_ascii();
	    cout << ' ';
	  }
	    else if (piece && !piece->is_white()) {
	      Terminal::color_fg(bright, Terminal::Color::BLACK);
	      cout << piece->to_ascii();
	      cout << ' ';
	    } else {
	  cout << ' ';
	  cout << ' ';
	  }
	}
      Terminal::color_fg(bright, Terminal::Color::DEFAULT_COLOR);
      Terminal::color_bg(Terminal::Color::DEFAULT_COLOR);
      cout << endl;
    }
}

/* legal_path function returns true if the start and end function are valid */
bool Board::legal_path(pair< char, char> start, pair< char, char> end) const {
  return valid_pos(start) && valid_pos(end);
}

/* is_opp function returns true if the piece at the end location is an opponent */
bool Board::is_opp(pair< char, char> start, pair< char, char> end) const{
  conconItr itstart = _occ.find(start);
  conconItr itend = _occ.find(end);
  return ((itstart->second)->is_white() != (itend->second)->is_white());
}

/*nonstraight function returns true if the piece path is not straight */
bool Board::nonstraight(pair< char, char> start, pair< char, char> end) const{
  if (start.first == end.first) {
    return false;
  } else if (start.second == end.second) {
    return false;
  } else if (abs(start.first - end.first) == abs(start.second - end.second)) {
    return false;
  }
  return true;
}

/* clear_path function returns true if there are nno pieces on the indicated path */
bool Board::clear_path(pair< char, char> start, pair< char, char> end) const{
  if (start.first == end.first) {
    if (start.second == end.second) { //guard against the floating exception
      return false;
    }
    int row_dir = (end.second - start.second) / abs(end.second - start.second);
    for (int i = 1; i < abs(end.second - start.second); i++) {
      if (have_piece(make_pair(start.first, start.second + row_dir * i)) != NULL) {
	return false;
      }
    }
  } else if (start.second == end.second) {
    int col_dir = (end.first - start.first)/ abs(end.first - start.first);
    for (int i = 1; i < abs(end.first - start.first);i++) {
      
      if (have_piece(make_pair(start.first + col_dir * i, start.second)) != NULL) {
        return false;
      }
    }
  } else {
    int col_dir = (end.first - start.first)/abs(end.first - start.first);
    int row_dir = (end.second - start.second)/abs(end.second - start.second);
    for (int i = 1; i < abs(end.first - start.first);i++) {
      if (have_piece(make_pair(start.first + col_dir * i, start.second + row_dir * i)) != NULL) {
	return false;
      }
    }
  }
  return true;
}

/*all legal function returns true if in 2 scenarios:
 1. If no piece in end position: there exists valid start piece with legal_path and legal_move_shape
 2. If there is piece in end position: there exists valid start piece with legal_capture_shape and clear_path if moving in straight path 
 */
bool Board::all_legal(pair<char, char> start, pair<char, char> end) const {
  const Piece* actual_piece = have_piece(start);
  if (actual_piece != NULL) {
    if (legal_path(start, end)) {
      if (start != end) {
	if (have_piece(end) == NULL) {
	  if (actual_piece->legal_move_shape(start, end)) {
	    if (clear_path(start,end)) {
	      // cout << "clear path" << endl;
	      return true;
	    }
	    else {
	      return false;
	    }
	  }
	} else {
	  if (is_opp(start,end)) {
	    if (actual_piece->legal_capture_shape(start,end)) {
	      if (!nonstraight(start,end)) {
		if (clear_path(start,end)) {
		  return true;
		}
		else {
		  return false;
		}
	      } else {
		return true;
	      }
	    }
	  }
	}
      }
    }
  }
  return false;
}

/* loc_king function returns the location of */
pair< char, char> Board::loc_king(bool white) const {
  for (conconItr it = _occ.cbegin(); it != _occ.cend(); ++it) {
    if (((it->second)->is_white() == white) && (toupper((it->second)->to_ascii()) == 'K')) {
      return it->first;
    }
  }
  return make_pair('?','?');
}

/* locations of all the pieces that the current player has */
map<pair< char, char>, Piece*> Board::al(bool white) const{
  map<pair< char, char>, Piece*> al_occ;
  for (conconItr it = _occ.cbegin(); it != _occ.cend(); ++it) {
    if ((it->second)->is_white() == white) {
      al_occ[it->first] = it->second;
    }
  }
  return al_occ;
}

/* locations of all the pieces the opposing player has */
map<pair< char, char>, Piece*> Board::op(bool white) const{
  map<pair< char, char>, Piece*> op_occ;
  for (conconItr it = _occ.cbegin(); it != _occ.cend(); ++it) {
    if ((it->second)->is_white() != white) {
      op_occ[it->first] = it->second;
    }
  }
  return op_occ;
}

/* indicate locations between two points, the path has to be a straight path */
vector<pair< char, char>> Board::loc_between(pair<char, char> start, pair<char, char> end) const{
  vector<pair<char, char>> result;
  if (start.first == end.first) {
    int row_dir = (end.second - start.second)/abs(end.second - start.second);
    for (int i = 1; i < abs(end.second - start.second); i++) {
      result.push_back(make_pair(start.first, start.second + row_dir * i));
    }
  } else if (start.second == end.second) {
    int col_dir = (end.first - start.first)/abs(end.first - start.first);
    for (int i = 1; i < abs(end.first - start.first); i++) {
      result.push_back(make_pair(start.first + col_dir * i, start.second));
    }
  } else {
    int col_dir = (end.first - start.first)/abs(end.first - start.first);
    int row_dir = (end.second - start.second)/abs(end.second - start.second);
    for (int i = 1; i < abs(end.first - start.first);i++) {
      result.push_back(make_pair(start.first + col_dir * i, start.second + row_dir * i));
    }
  }
  return result;
}

//functions helps make_move
/* check if the mystery piece will get itself in check by moving one of its own chess piece*/
bool Board::mystery_fake_move(pair<char, char> start, pair<char, char> end) {
  bool result;
  vector<pair<char, char>> get_removed_loc;
  vector<char> get_removed_desig;
  //legal move
  if (!have_piece(end)) {
    add_piece(end, _occ[start]->to_ascii());
    get_removed_loc.push_back(start);
    get_removed_desig.push_back(_occ[start]->to_ascii());
    remove_piece(start);
    result = !in_check(_occ[end]->is_white());
    add_piece(get_removed_loc[0], get_removed_desig[0]);
    remove_piece(end);
  }
  //legal capture
  else {
    int count = 0;
    for (conItr it = _occ.begin(); it != _occ.end(); ++it) {
      if ((it->second) ->to_ascii() == _occ[start]->to_ascii()) {
	count++;
	get_removed_loc.push_back(it->first);
	get_removed_desig.push_back(it->second->to_ascii());
      }
    }
    get_removed_loc.push_back(end);
    get_removed_desig.push_back(_occ[end]->to_ascii());
    if (count == 1) {
      char piece_desig;
      if (get_removed_desig[0] == 'm') {
	piece_desig = 'M';
      } else {
	piece_desig = 'm';
      }
      remove_piece(start);
      remove_piece(end);
      add_piece(end, piece_desig);
      result = !in_check(_occ[end]->is_white());
      remove_piece(end);
      add_piece(get_removed_loc[0], get_removed_desig[0]);
      add_piece(get_removed_loc[1], get_removed_desig[1]);
    } else if (count == 2) {
      bool color = _occ[start]->is_white();
      for (size_t i = 0; i < get_removed_loc.size(); i++) {
	remove_piece(get_removed_loc[i]);
      }
      result = !in_check(color);
      for (size_t i = 0; i < get_removed_loc.size(); i++) {
	add_piece(get_removed_loc[i], get_removed_desig[i]);
      }
    }
  }
  return result;
}

/*actual move for mystery piece*/
void Board::mystery_move(pair<char, char> start, pair<char, char> end) {
  //legal move
  if (!have_piece(end)) {
    add_piece(end, _occ[start]->to_ascii());
    remove_piece(start);
  }
  //legal capture
  else {
    int count = 0;
    vector<pair<char, char>> get_removed_loc;
    vector<char> get_removed_desig;
    for (conItr it = _occ.begin(); it != _occ.end(); ++it) {
      if ((it->second) ->to_ascii() == _occ[start]->to_ascii()) {
        count++;
	get_removed_loc.push_back(it->first);
      }
    }
    //if this player who is making the move have only one mystery piece, it captures the
    // opposing piece and its own mystery piece becomes the opposing's after the move
    if (count == 1) {
      char piece_desig;
      if (_occ[start]->to_ascii() == 'm') {
        piece_desig = 'M';
      } else {
        piece_desig = 'm';
      }
      remove_piece(start);
      remove_piece(end);
      add_piece(end, piece_desig);
      //if this player who is making the move have two mystery pieces (both sides used the
      //mystery piece for an attack, the mystery pieces disappers from the board
    } else if (count == 2) {
      remove_piece(get_removed_loc[0]);
      remove_piece(get_removed_loc[1]);
      remove_piece(end);
    }
  }
}

/* test if a pawn will get promoted without being checked */
bool Board::pawn_fake_promotion(pair<char, char> start, pair<char, char> end) {
  bool result;
  char piece_desig;
  if (_occ[start]->is_white()) {
    piece_desig = 'Q';
  } else {
    piece_desig = 'q';
  }
  vector<pair<char,char>> get_removed_loc;
  vector<char> get_removed_desig;
  //if (_occ[start]->legal_move_shape(start, end)) {
  if (!have_piece(end)) {
    get_removed_loc.push_back(start);
    get_removed_desig.push_back(_occ[start]->to_ascii());
    add_piece(end, piece_desig);
    remove_piece(start);
    result = !in_check(_occ[end]->is_white());
    remove_piece(end);
    add_piece(get_removed_loc[0], get_removed_desig[0]);
  } else {
    get_removed_loc.push_back(start);
    get_removed_desig.push_back(_occ[start]->to_ascii());
    get_removed_loc.push_back(end);
    get_removed_desig.push_back(_occ[end]->to_ascii());
    remove_piece(end);
    remove_piece(start);
    add_piece(end, piece_desig);
    result = !in_check(_occ[end]->is_white());
    remove_piece(end);
    add_piece(get_removed_loc[0], get_removed_desig[0]);
    add_piece(get_removed_loc[1], get_removed_desig[1]);
  }
  return result;
}

//Attmpt to promote the pawn
void Board::pawn_promotion(pair<char, char> start, pair<char, char> end) {
  char piece_desig;
  if (_occ[start]->is_white()) {
    piece_desig = 'Q';
  } else {
    piece_desig = 'q';
  }
  if (_occ[start]->legal_move_shape(start, end)) {
    add_piece(end, piece_desig);
    remove_piece(start);  
  } else {
    remove_piece(end);
    remove_piece(start);
    add_piece(end, piece_desig);
  }
}

// functions for checked conditions, checkmate and stalemate
bool Board::in_mate(bool white) const
{
  pair<char,char> checked_king=loc_king(white); 
  if(!checking(checked_king,white)) {
    //cout << "not in check" << endl;
    return false;
  }
  //not even in checking condition, exit immediately
  //check the position around the king
  if(!round_checking(checked_king,white)) {
    //cout << "not round checking" << endl;
    return false;
  }
  //king can move to save himself
  //right now, the king can't move
  //check is there more than one piece are checking the king now
  if(multi_checking(checked_king,white))
    {
      return true;
    }
  //cout << "not multi checking" <<endl;
  //if more than one piece is checking the king, it's checkmate,break
  //check if your piece can capture the killer
  if(capture_killer(checked_king,white))
    {
      //cout << "capture_killer" << endl;
      return false;
    }
  //guards can capture the killer. not a checkmate
  //check if your piece can block the piece at the path
  if(block_path(checked_king,white))
    {
      //cout << "block_path" << endl;
      return false;
    }
  //guards can go block the killer's path
  return true;
}

bool Board::in_check(bool white) const
{
  pair<char,char> checked_king = loc_king(white);//get the location of the checked king
  if (checking(checked_king, white) == false) {
    return false;
    
  } else {
    return true;
  }
}
  
bool Board::checking(pair<char,char>king,bool white) const {
  map<pair<char,char>, Piece*> killers = op(white); 
  for(map<pair<char,char>,Piece*>::iterator it = killers.begin(); it!=killers.end(); ++it) {
    if(it->second->legal_capture_shape(it->first, king)) {
      if (!nonstraight(it->first, king) && clear_path(it->first, king)) {
	return true;
      } else if (nonstraight(it->first, king)) {
	return true;
      } 
    }
  }//can be captured by someone
  return false;
}

bool Board::round_checking(pair<char,char>king,bool white) const {
  //return true if got checked all around
  pair<char,char> lower_row = make_pair(king.first-1,king.second-1);
  pair<char, char> temp;
  for(int i=0; i < 3; i++) {
    for(int j=0;j<3; j++){
      temp = make_pair(lower_row.first + i, lower_row.second + j);
      /*if position is empty and no piece is checking that, not checkmate
       or if the position is occupied by other's piece, king can capture it
       if the position is occupied by my pieces, king cannot be moved
      */
      if (valid_pos(temp)) {
	if ((have_piece(temp) == NULL) || (white != have_piece(temp)->is_white())) {
	  if (!checking(temp, white)) {
	    return false;
	  }
	} 
      }
    }
  }
  return true;
}

bool Board::multi_checking(pair<char,char>king,bool white) const
{
  int occ=0;
  map<pair<char,char>,Piece*> killers = op(white);
  for(map<pair<char,char>,Piece*>::iterator it = killers.begin(); it!=killers.end(); ++it){
    if(it->second->legal_capture_shape(it->first,king)) {
      if ((!nonstraight(it->first,king)&&clear_path(it->first,king)) || nonstraight(it->first,king)){
	occ++;
      }
    }//can be captured by someone
  }
  if(occ>1){return true;}
  return false;
}

pair<char,char> Board::killer_pos(pair<char,char>king,bool white) const {
  map<pair<char,char>,Piece*> killers = op(white);
  for(map<pair<char,char>,Piece*>::iterator it = killers.begin(); it!=killers.end(); ++it) {
    if(it->second->legal_capture_shape(it->first,king)) {
      if ((!nonstraight(it->first,king)&&clear_path(it->first,king)) || nonstraight(it->first,king)){
      return it->first;
      }//can be captured by someone
    }
  }
  return make_pair('?','?');
}

bool Board::capture_killer(pair<char,char>king,bool white) const {
  pair<char,char>killer = killer_pos(king,white);
  map<pair<char,char>,Piece*> guards = al(white);
  for(map<pair<char,char>,Piece*>::iterator it = guards.begin(); it!=guards.end(); ++it) {
    if(it->second->legal_capture_shape(it->first, killer)) {
      if ((!nonstraight(it->first,killer) &&(clear_path(it->first,killer))) || nonstraight(it->first,killer)) {
	if (toupper(it->second->to_ascii()) == 'K') {
	  if (!checking(killer,white)) {
	    return true;
	  }
	} else {
	  return true;
	}//guards can captured the killer
      }
    }
  }
  return false;
}

bool Board::block_path(pair<char,char>king, bool white) const {
  map<pair<char,char>,Piece*> guards = al(white);
  pair<char,char> killer = killer_pos(king,white);
  vector<pair<char,char>> block_pos = loc_between(killer,king);
  for (size_t i = 0; i < block_pos.size(); i++) {
    for(map<pair<char,char>,Piece*>::iterator mit = guards.begin(); mit!=guards.end(); ++mit) {
      if (mit->second->legal_move_shape(mit->first,block_pos[i])) {
	if ((!nonstraight(mit->first,block_pos[i])&&(clear_path(mit->first,block_pos[i]))) || nonstraight(mit->first,block_pos[i])) {
	  if (toupper(mit->second->to_ascii()) == 'K') {
	    if (!checking(block_pos[i],white)) {
	      return true;
	    }
	  } else {
	    return true;
	  }//guards can go block the killer's path
	}
      }
    }
  }
  return false;
}

//DO NOT MODIFY THIS FUNCTION
std::ostream& operator << ( std::ostream& os , const Board& board) {
	for( char r='8' ; r>='1' ; r-- )
	{	  
	  for( char c='A' ; c<='H' ; c++ )
	    {
	      const Piece* piece = board( std::pair< char , char >( c , r ) );
	      if( piece ) os << piece->to_ascii();
	      else        os << '-';
	    }
	  os << std::endl;
	}
	return os;
}
