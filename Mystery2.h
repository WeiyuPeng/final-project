#ifndef MYSTERY_H
#define MYSTERY_H

#include <utility>
#include <cmath>
#include "Piece.h"
//Mystery piece that can move 2 right/left and 3 up/down
class Mystery : public Piece
{
public:
        bool legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const
        {
	  if ((abs(end.first - start.first) == 2) && (abs(end.second - start.second) == 3)) {
            return true;
          }

                return false;
        }
        /*legal_capture_shape( std::pair< char , char > start , std::pair< char , char > end) const {
   if(is_white() == true && abs(end.first - start.first) == 2) {
    return true;
  }
  //black pawn try to capture,moving diagonally by one block
  if(is_white() == false&&abs(end.second - start.second) == 4) {
    return true;
  }
  return false;
}
*/
        char to_ascii( void ) const { return is_white() ? 'M' : 'm'; }
        

private:
        Mystery( bool is_white ) : Piece( is_white ) {}

        friend Piece* create_piece( char );
};

#endif // MYSTERY

