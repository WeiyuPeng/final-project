//Pawn.cpp

#include <iostream>
#include <utility>
#include "Pawn.h"

using std::get;
using std::pair;
using std::abs;

//helper function will call move if there's no piece at the end
//if there's opposing piece at the end, helper function will call capture instead

bool Pawn::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {

  if(get<0>(start) == get<0>(end)) {
    //check if it is the white pawn's move
    if(is_white() == true) {
      /*return true if white pawn hasn't moved yet and tries to move forward 1 or 2 sp \
	aces*/
      if(get<1>(start)=='2') {
	if(((get<1>(end)-get<1>(start)) > 0) && ((get<1>(end)-get<1>(start)) <= 2)) {
	  return true;
	}
      }
      //return true if not first move and pawn only tries to move forward 1 space
      else {
	if(((get<1>(end)-get<1>(start)) > 0) && ((get<1>(end)-get<1>(start)) <= 1)) {
	  return true;
	}
      }
    }
    if(is_white() == false) {
      if (get<1>(start)=='7') {
	/*true if white pawn hasn't moved yet and tries to move forward 1 or 2 space \
	  s*/
	if (((get<1>(start)-get<1>(end)) > 0) && ((get<1>(start) - get<1>(end)) <= 2)) {
	  return true;
	}
      }
      //return true if not first move and pawn only tries to move forward 1 space
      else {
	if(((get<1>(start)-get<1>(end)) > 0) && ((get<1>(start)-get<1>(end))<=1)) {
	  return true;}
      }
    }
    
  } //end of forward direction check
  return false;
}


bool Pawn::legal_capture_shape( std::pair< char , char > start , std::pair< char , char > end) const {
  //white pawn try to capture,moving diagonally by one block
  if(is_white() == true && abs(get<0>(start)-get<0>(end)) == 1&&get<1>(end)-get<1> \
     (start) == 1) {
    return true;
  }
  //black pawn try to capture,moving diagonally by one block
  if(is_white() == false&&abs(get<0>(start)-get<0>(end))==1&&get<1>(start)-get<1>(end)==1) {
    return true;
  }
  return false;
}

