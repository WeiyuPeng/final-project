//Queen.cpp

#include "Piece.h"
#include "Queen.h"
#include <cmath>

bool Queen::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  //return true if trying to move diagonal
  if (abs(end.second-start.second) == abs(end.first-start.first)) {
    return true;
  }
  //return true if trying to move straight
  if (end.first == start.first) {
    return true;
  }
  if (end.second == start.second) {
    return true;
  }
  return false;
}
