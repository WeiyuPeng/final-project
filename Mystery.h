#ifndef MYSTERY_H
#define MYSTERY_H

#include <utility>
#include <cmath>
#include "Piece.h"
//Mystery piece that can move 2 right/left and 4 up/down
class Mystery : public Piece
{
public:
        bool legal_move_shape( std::pair< char , char > start , std::pair< c\
har , char > end ) const
        {
	   if (abs(end.first - start.first) == 2) {
            return true;
          }
          if (abs(end.second - start.second) == 4) {
            return true;
          }

                return false;
        }

        char to_ascii( void ) const { return is_white() ? 'M' : 'm'; }

private:
        Mystery( bool is_white ) : Piece( is_white ) {}

        friend Piece* create_piece( char );
};

#endif // MYSTERY
