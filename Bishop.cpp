//Bishop.cpp

#include <iostream>
#include <utility>
#include <cmath>
#include "Bishop.h"

using std::get;
using std::pair;
using std::abs;

bool Bishop::legal_move_shape(pair< char , char > start , pair< char , char > end ) const {

   //return true as long as the piece moves same portion vertically and horizontally
   if(abs(get<0>(start)-get<0>(end)) == abs(get<1>(start)-get<1>(end))) {
      return true;
   }
   return false;
}
