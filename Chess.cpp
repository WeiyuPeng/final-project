
#include "Chess.h"
#include "Board.h"
#include <iostream>
#include <vector>
#include <utility>

using std::pair;
using std::vector;


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess( void ) : _turn_white( true ) {
  // Add the pawns
  for( int i=0 ; i<8 ; i++ )
    {
      _board.add_piece( std::pair< char , char >( 'A'+i , '1'+1 ) , 'P' );
      _board.add_piece( std::pair< char , char >( 'A'+i , '1'+6 ) , 'p' );
    }
  
  // Add the rooks
  _board.add_piece( std::pair< char , char >( 'A'+0 , '1'+0 ) , 'R' );
  _board.add_piece( std::pair< char , char >( 'A'+7 , '1'+0 ) , 'R' );
  _board.add_piece( std::pair< char , char >( 'A'+0 , '1'+7 ) , 'r' );
  _board.add_piece( std::pair< char , char >( 'A'+7 , '1'+7 ) , 'r' );
  
  // Add the knights
  _board.add_piece( std::pair< char , char >( 'A'+1 , '1'+0 ) , 'N' );
  _board.add_piece( std::pair< char , char >( 'A'+6 , '1'+0 ) , 'N' );
  _board.add_piece( std::pair< char , char >( 'A'+1 , '1'+7 ) , 'n' );
  _board.add_piece( std::pair< char , char >( 'A'+6 , '1'+7 ) , 'n' );
  
  // Add the bishops
  _board.add_piece( std::pair< char , char >( 'A'+2 , '1'+0 ) , 'B' );
  _board.add_piece( std::pair< char , char >( 'A'+5 , '1'+0 ) , 'B' );
  _board.add_piece( std::pair< char , char >( 'A'+2 , '1'+7 ) , 'b' );
  _board.add_piece( std::pair< char , char >( 'A'+5 , '1'+7 ) , 'b' );
  
  // Add the kings and queens
  _board.add_piece( std::pair< char , char >( 'A'+3 , '1'+0 ) , 'Q' );
  _board.add_piece( std::pair< char , char >( 'A'+4 , '1'+0 ) , 'K' );
  _board.add_piece( std::pair< char , char >( 'A'+3 , '1'+7 ) , 'q' );
  _board.add_piece( std::pair< char , char >( 'A'+4 , '1'+7 ) , 'k' );
  
}

//Attempt to make a move to see if the player will get him/herslef in check
bool Chess::make_fake_move(std::pair<char, char> start, std::pair<char, char> end) {
  bool result;
  if (toupper((_board)(start)->to_ascii()) == 'M') {
    result =  _board.mystery_fake_move(start,end);
  } else if ((toupper((_board)(start)->to_ascii()) == 'P') && (end.second == '1' || end.second == '8')) {
    result =  _board.pawn_fake_promotion(start,end);
  } else {
    vector<pair<char, char>> get_removed_loc;
    vector<char> get_removed_desig;
    if (!_board.have_piece(end)) {
      get_removed_loc.push_back(start);
      get_removed_desig.push_back((_board)(start)->to_ascii());
      _board.add_piece(end, get_removed_desig[0]);
      _board.remove_piece(start);
      result = !_board.in_check((_board)(end)->is_white());
      _board.remove_piece(end);
      _board.add_piece(get_removed_loc[0], get_removed_desig[0]);
    } else {
      get_removed_loc.push_back(start);
      get_removed_desig.push_back((_board)(start)->to_ascii());
      get_removed_loc.push_back(end);
      get_removed_desig.push_back((_board)(end)->to_ascii());
      _board.remove_piece(end);
      _board.remove_piece(start);
      _board.add_piece(end, get_removed_desig[0]);
      result = !_board.in_check((_board)(end)->is_white());
      _board.remove_piece(end);
      _board.add_piece(get_removed_loc[0], get_removed_desig[0]);
      _board.add_piece(get_removed_loc[1], get_removed_desig[1]);
    }
  }
  return result;
}

//Attempt to make an actual move
bool Chess::make_move( std::pair< char , char > start , std::pair< char , char > end )
{
  if (!_board.valid_pos(start) || !_board.valid_pos(end)) {
    return false;
  }
  if (_board.have_piece(start) == NULL) {
    return false;
  }
  if (_turn_white != (_board)(start)->is_white()) {
    return false;
  }
  if (!_board.all_legal(start,end)) {
    return false;
  }
  if (toupper((_board)(start)->to_ascii()) == 'M') {
    if (!_board.mystery_fake_move(start,end)) {
      return false;
    }
    _board.mystery_move(start,end);
  } else if ((toupper((_board)(start)->to_ascii()) == 'P') && (end.second == '1' || end.second == '8')){
    if (!_board.pawn_fake_promotion(start,end)) {
      return false;
    }
    _board.pawn_promotion(start,end);
  } else {
    if (!make_fake_move(start,end)) {
      return false;
    }
    vector<pair<char, char>> get_removed_loc;
    vector<char> get_removed_desig;
    if (!_board.have_piece(end)) {
      get_removed_desig.push_back((_board)(start)->to_ascii());
      _board.add_piece(end, get_removed_desig[0]);
      _board.remove_piece(start);
    } else {     
      get_removed_desig.push_back((_board)(start)->to_ascii());
      _board.remove_piece(end);
      _board.remove_piece(start);
      _board.add_piece(end, get_removed_desig[0]);
    }
  }
  _turn_white = !_turn_white;
  return true;
}

//Check for in_check condition
bool Chess::in_check( bool white ) const
{
  return  _board.in_check(white);
}

//check for in checkmate condition
bool Chess::in_mate( bool white ) const
{
  return  _board.in_mate(white);
}

//check for stalemate condition
bool Chess::in_stalemate( bool white ) 
{
  std::map<pair<char,char>,Piece*> killers=_board.al(white);
  for(std::map<pair<char,char>,Piece*>::iterator it=killers.begin();it!=killers.end();++it) {
    for(int j=0;j<8;j++) {
      for(int i=0;i<8;i++) {
        pair<char,char>temp=std::make_pair('A'+i,'1'+j);
	if (_board.valid_pos(it->first)) {
	  if(_board.all_legal(it->first,temp)==true) {
	    if (make_fake_move(it->first,temp)==true) {
	      return false;
	    } else {
	      return true;
	    }
	  }
	}
      }
    }
  }
  return true;
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Chess& chess )
{
  // Write the board out and then either the character 'w' or the character 'b', depending on whose turn it is
	return os << chess.board() << ( chess.turn_white() ? 'w' : 'b' );
}


//load a board in
std::istream& operator >> ( std::istream& is, Chess& chess){
  for (char r = '8'; r >= '1'; r--) {
    for (char c = 'A'; c <= 'H'; c++) {
      if (chess._board.have_piece(std::make_pair(c,r)) != NULL) {
	chess._board.remove_piece(std::make_pair(c,r));
      }
    }
  }
  for( char r='8' ; r>='1' ; r-- ) {
    for(char c = 'A' ; c <= 'H' ; c++ ) {
      char file_chars;
      is >> file_chars;
      if(file_chars != '-') {
	chess._board.add_piece(std::make_pair(c, r), file_chars);
      }
    }		 
  }
  char who;
  is >> who;
  if (who == 'w') {
    chess._turn_white = true;
  } else if (who == 'b') {
    chess._turn_white = false;
  }
  return is;
}

